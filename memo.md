# メモ
### 20210404_ラジオ
* [↑上記タイムテーブル](https://www.fmyokohama.co.jp/pc/program/BaysideReggaeLounge2)
* ニュールーツ系アーティスト
  * URBAN RITES／THE SHANTI ITES
  * NATURAL ROOTS／EARL SIXTEEN
  * KIBOU NO HIKARI／[CHAZBO & YURI BAMBOO](https://www.jetsetrecords.net/ojah-resilience-feat-chazbo-yuuri-bamboo/i/535005831359/)
  * MIGHTY SPINX／[THE BUSH CHEMISTS](https://diskunion.net/portal/ct/search?m=11&q=THE+BUSH+CHEMISTS&submit.x=0&submit.y=0)
  * RUDE BWOY FACE／JOHN JUNIOR
  * THE WARRIOR QUEST／[HIGHER MEDITATION MEETS REMEDI](https://soundcloud.com/highermeditation/higher-meditation-meets-1)
  * KING MUSIC／[DUBKASM](http://www.discshopzero.com/item/sufferahschoice.html)
### 20210401_本
* [REGGAE definitive (ele-king books) ](https://www.amazon.co.jp/gp/product/4909483667?pf_rd_r=CH3YSW254DPHDM76MKDF&pf_rd_p=7626af39-b716-47c8-84eb-9679f177dc53&pd_rd_r=8a89df14-8763-479e-9dbc-027594be7810&pd_rd_w=RHPbW&pd_rd_wg=xWV5X&ref_=pd_gw_unk)