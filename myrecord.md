## 所有音源まとめ

### 目次
1. [レコード一覧](#レコード一覧)  
1. [CD一覧](#cd一覧)

### レコード一覧  
アーティスト順

* Al campbell  
The Al campbell collection  
  <img src="./artwork/record/The%20Al%20campbell%20collection.jpg" width="200px">

* Alton ellis  
The best of Alton ellis  
  <img src="./artwork/record/The%20best%20of%20Alton%20ellis.jpg" width="200px">

* Alpha & Omega  
Trample the eagle and the doragon and the bear  

* Augustus Pablo  
Raggamuffin dub  
  <img src="./artwork/record/Raggamuffin%20dub.jpg" width="200px">  
Eastman dub(2018)  
Blowing with the wind  
  <img src="./artwork/record/Blowing%20with%20the%20wind.jpg" width="200px">

* Black slate  
Ogima  
  <img src="./artwork/record/Ogima.jpg" width="200px">

* Bunny Wailer  

* Don Drummond  
the best of Don Drummond  
  <img src="./artwork/record/the%20best%20of%20Don%20Drummond.jpg" width="200px">

* Gladstone anderson and the Mudies all stars  
glady unlimited

* Hugh Mindel  
Time and place

* Horace Andy  
Dance Hall Style  
  <img src="./artwork/record/Dance%20Hall%20Style.jpg" width="200px">

* Jackie Mittoo  
Reggae magic!(1972)  
  <img src="./artwork/record/Reggae%20magic!.jpg" width="200px">  
Keyboard king  
Evening time(1967)  
Money marker  
Now  
Show case volume3  
In cold  blood  
  <img src="./artwork/record/In%20Cold%20Blood.jpg" width="200px">  
Hot blood  
  <img src="./artwork/record/HOT%20BLOOD.jpg" width="200px">  
In london  
The keyboard king at studio one  

* Jah Shaka  
COMMANDMENTS OF DUB 10 AFRICA DRUM BE  
  <img src="./artwork/record/COMMANDMENTS%20OF%20DUB%2010%20AFRICA%20DRUM%20BE.jpg" width="200px">  
New Testaments Of Dub 1  
  <img src="./artwork/record/New%20Testaments%20Of%20Dub%201.jpg" width="200px">  
Brimstone & Fire  
  <img src="./artwork/record/Brimstone%20&%20Fire.jpg" width="200px">  
The Disciples  
  <img src="./artwork/record/The%20Disciples.jpg" width="200px">  
Lion's Share Of Dub (Commandments Of Dub Part 3)  
  <img src="./artwork/record/Lion's%20Share%20Of%20Dub%20(Commandments%20Of%20Dub%20Part%203).jpg" width="200px">  
DUB SYMPHONY  
  <img src="./artwork/record/DUB%20SYMPHONY.jpg" width="200px">  

* Jonnie clarke  
Dread natty congo  
  <img src="./artwork/record/Dread%20natty%20congo.jpg" width="200px">  
Authorized version

* Lee "Scratch" Perry  

* Love Joys  
Lovers Rock Reggae Style: Love Joys Show Case (1982)  
  <img src="./artwork/record/Lovers%20Rock%20Reggae%20Style%20Love%20Joys%20Show%20Case%20(1982).jpg" width="200px">

* Matumbi  
the best of Matumbi

* Max Romeo  
FARI, CAPTAIN OF MY SHIP  
  <img src="./artwork/record/FARI,%20CAPTAIN%20OF%20MY%20SHIP.jpg" width="200px">

* Nik trop  
Reminiscence

* Ojah  
On my mind  
  <img src="./artwork/record/On%20my%20mind.jpg" width="200px">

* PRINCE ALLA  
JAH CHILDREN GATHER ROUND  
  <img src="./artwork/record/Jah%20Children%20Gather%20Round.jpg" width="200px">

* Rico & the rudies  
blow your horn  
  <img src="./artwork/record/blow%20your%20horn.jpg" width="200px">

* Roland Alphonso  
ABC rocksteady  
Best of Roland Alphonso  
  <img src="./artwork/record/Best%20of%20Roland%20Alphonso.jpg" width="200px">

* Roots Underground  
Tribesman Assault  
  <img src="./artwork/record/Tribesman%20Assault.jpg" width="200px">

* Skatalites  
Skatalites & friends at Randy’s  
Ska authentic  
  <img src="./artwork/record/Ska%20authentic.jpg" width="200px">

* SOUL SYNDICATE  
HARVEST UPTOWN  
  <img src="./artwork/record/R-2556434-1572660889-1310.jpg" width="200px">  
WAS.IS & ALWAYS  
  <img src="./artwork/record/R-2014136-1258578020.jpg" width="200px">

* Third world  
Journey to Addis  
  <img src="./artwork/record/Journey%20to%20Addis.jpg" width="200px">

* Tommy Mccoom & The Agrovators  
King tubby meets the agrovators at dub station  
Super star  
  <img src="./artwork/record/Super%20star.jpg" width="200px">  
Cookin'

* Tommy Mccoom & the super sonic  
Top secret  
  <img src="./artwork/record/Top%20secret.jpg" width="200px">  

* Tommy Mccoom  
Tommy Mccoom

* Trojans  
SAVE THE WORLD  
  <img src="./artwork/record/SAVE%20THE%20WORLD.jpg" width="200px">

* U ROY  
Crucial cuts

* Wackie's  
African roots act1  
  <img src="./artwork/record/African%20roots%20act1.jpg" width="200px">  
African Roots Act 3  
  <img src="./artwork/record/African%20Roots%20Act%203.jpg" width="200px">  
Creation Dub  
  <img src="./artwork/record/Creation%20Dub.jpg" width="200px">

* Wayne Jarrett  
Showcase Volume 1: Bubble Up  
  <img src="./artwork/record/Showcase%20Volume%201%20Bubble%20Up.jpg" width="200px">

* WINSTON EDWARDS & BLACKBEARD  
Dub Conference At 10 Downing Street  
  <img src="./artwork/record/Dub%20Conference%20At%2010%20Downing%20Street.jpg" width="200px">

* V.A.  
Ja son invasion  
This is Jamaica ska  
Reggae goodies vol.1  
  <img src="./artwork/record/Reggae%20goodies%20vol.1.jpg" width="200px">  
Java

### CD一覧
アーティスト順

* Abyssinians  
Arise [Reissue]  
  <img src="./artwork/cd/Arise%20%5BReissue%5D.jpg" width="200px">

* African Head Charge  
Vision Of A Psychedelic Africa [Bonus Track]

* Alpha & Omega  
Sound System Dub  
  <img src="./artwork/cd/Sound%20System%20Dub.jpg" width="200px">

* Althea & Donna  
Uptown Top Ranking  

* Alton Ellis  
I'm Still In Love With You; Featuring Hortense Ellis

* Alton Ellis With Dreamlets & Naoyuki Uchida  
Lovely Place

* audio active  
Back To The Stoned Age  
  <img src="./artwork/cd/Back%20To%20The%20Stoned%20Age.jpg" width="200px">

* Augustus Pablo  
Dub, Reggae and Roots from the Melodica King  
East Of The River Nile

* Aswad  
New Chapter Of Dub

* Black Uhuru  
Red  
  <img src="./artwork/cd/Red.jpg" width="200px">  
Sinsemilla  
  <img src="./artwork/cd/Sinsemilla.jpg" width="200px">

* Bob Marley & The Wailers  
Burnin'  
  <img src="./artwork/cd/Burnin'.jpg" width="200px">  
Catch A Fire  
Exodus  
  <img src="./artwork/cd/Exodus.jpg" width="200px">  
Live!  
  <img src="./artwork/cd/Live!.jpg" width="200px">

* Bunny Wailer  
Blackheart Man  
  <img src="./artwork/cd/Blackheart%20Man.jpg" width="200px">  
Bunny Wailer Sings The Wailers  
  <img src="./artwork/cd/Bunny%20Wailer%20Sings%20The%20Wailers.jpg" width="200px">

* Burning Spear  
Dry & Heavy + Man In The Hills

* Creation Rebel  
Rebel Vibrations  
  <img src="./artwork/cd/Rebel%20Vibrations.jpg" width="200px">  
Starship Africa  
  <img src="./artwork/cd/Starship%20Africa.jpg" width="200px">

* Congos  
Heart Of The Congos  
  <img src="./artwork/cd/Heart%20Of%20The%20Congos.jpg" width="200px">

* Cornell Campbell  
Sweet Dancehall Collection  
  <img src="./artwork/cd/Sweet%20Dancehall%20Collection.jpg" width="200px">

* Dennis Bovell  
I Wah Dub  
The British Core Lovers  
  <img src="./artwork/cd/The%20British%20Core%20Lovers.jpg" width="200px">

* DETERMINATIONS  
Rock A Shacka Vol.1 -Prince Buster With Determinations Live In Japan

* Dry & Heavy  
FULL CONTACT  
  <img src="./artwork/cd/FULL%20CONTACT.jpg" width="200px">

* Gladiators  
Proverbial Reggae

* Gregory Isaacs  
Soon Forward  
  <img src="./artwork/cd/Soon%20Forward.jpg" width="200px">

* Heptones  
Nightfood Ina Party Time  
  <img src="./artwork/cd/Nightfood%20Ina%20Party%20Time.jpg" width="200px">

* Icebreakers With The Diamonds  
Planet Mars In Dub

* Inner Circle  
Ready For The World

* Jackie Mittoo  
Drum Song  
  <img src="./artwork/cd/Drum%20Song.jpg" width="200px">

* Jah Shaka  
In the Ghetto  
  <img src="./artwork/cd/In%20the%20Ghetto.jpg" width="200px">

* Jimmy Cliff  
Best Of Jimmy Cliff  
  <img src="./artwork/cd/Best%20Of%20Jimmy%20Cliff.jpg" width="200px">

* Joe Gibbs & The Professionals  
State Of Emergency  
  <img src="./artwork/cd/State%20Of%20Emergency.jpg" width="200px">  
African Dub All-Mighty Chapter 4  
  <img src="./artwork/cd/African%20Dub%20All-Mighty%20Chapter%204.jpg" width="200px">

* Johnny Clarke  
Roots Music  
  <img src="./artwork/cd/Roots%20Music.jpg" width="200px">

* Kiddus I  
Kiddus I Meets Reggaelation Independance

* King Tubby  
King Tubby On The Mix Vol. Two

* Lee "Scratch" Perry  
Cutting Razor: Rare Cuts From the Black Ark

* Lee "Scratch" Perry & The Upsetters  
Super Ape  
  <img src="./artwork/cd/Super%20Ape.jpg" width="200px">

* Little Tempo  
Kedaco Sounds  
  <img src="./artwork/cd/Kedaco%20Sounds.jpg" width="200px">

* Love Joys  
Reggae Vibes  
  <img src="./artwork/cd/Reggae%20Vibes.jpg" width="200px">

* Mad Professor  
Dub Me Crazy!!  
  <img src="./artwork/cd/Dub%20Me%20Crazy!!.jpg" width="200px">

* Max Romeo  
War Ina Babylon/Reconstruction  
  <img src="./artwork/cd/War%20Ina%20Babylon.jpg" width="200px">

* Mighty Diamonds  
Deeper Roots (Back At The Channel)  
  <img src="./artwork/cd/Deeper%20Roots%20(Back%20At%20The%20Channel).jpg" width="200px">

* Mute Beat  
Lover's Rock  
  <img src="./artwork/cd/Lover's%20Rock.jpg" width="200px">  
Still Echo  
LIVE【Remastered】  
  <img src="./artwork/cd/LIVE%E3%80%90Remastered%E3%80%91.jpg" width="200px">  
Dub Wise  
March  
  <img src="./artwork/cd/March.jpg" width="200px">  
Flower  
  <img src="./artwork/cd/Flower.jpg" width="200px">

* Peter Tosh  
Legalize It  
  <img src="./artwork/cd/Legalize%20It%20%5BDisc%202%5D.jpg" width="200px">

* Phyllis Dillon  
One Life To Live  
  <img src="./artwork/cd/One%20Life%20To%20Live.jpg" width="200px">

* Prince Douglas  
Dub Roots  
  <img src="./artwork/cd/Dub%20Roots.jpg" width="200px">

* Rico Rodigues
Warrika Dub  
Man From Wareika [Bonus Tracks]

* Scientist  
Scientist Rids The World Of The Evil Curse Of The Vampires

* Sherwood & Pinch  
Late Night Endless  
  <img src="./artwork/cd/Late%20Night%20Endless%20%5BBonus%20Track%5D.jpg" width="200px">

* Singers & Players  
Staggering Heights  
Revenge Of The Underdog  
War Of Words

* Sister Love  
Love Comes Love Goes

* Ska Flames  
Wail'n Skal'm

* Skatalites  
Hip-Bop Ska  
The Authentic Sound of Tommy Mccook  

* Steel Pulse  
Handsworth Revolution  
  <img src="./artwork/cd/Handsworth%20Revolution.jpg" width="200px">

* Sublime With Rome  
Sirens

* Third World  
96° In The Shade  
You've Got The Power

* Tradition  
Captain Ganja And The Space Patrol +1  
  <img src="./artwork/cd/81qRcDXtqZL._AC_SL1400.jpg" width="200px">  
Alternative Routes  
  <img src="./artwork/cd/81o3jOVk9RL._AC_SY355.jpg" width="200px">  
Moving on  
  <img src="./artwork/cd/000124-300x300.jpg" width="200px">

* U ROY  
Jah Son Of Africa  
  <img src="./artwork/cd/Dread%20In%20A%20Babylon.jpg" width="200px">  
Dread In A Babylon  
  <img src="./artwork/cd/Jah%20Son%20Of%20Africa.jpg" width="200px">

* Zion Train  
Zion Train: Siren

* V.A.  
DC Dub Connection  
Countryman  
Relaxin' With Lovers Volume 3 / Studio 16 Lovers Rock Collections  
RELAXIN' WITH JAPANESE LOVERS VOLUME 6 ～WE LOVE JAPANESE LOVERS MORE THAN EVER COLLECTIONS～

* こだま和文  
Quiet Reggae
